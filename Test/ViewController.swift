//
//  ViewController.swift
//  Test
//
//  Created by Timur Astashov on 7/10/18.
//  Copyright © 2018 Timur Astashov. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        // Block 1 Task 0
        print("-------Block 1 Task 0:-------")
        let firstNumber = 129081409
        let secondNumber = 3939499093
        if firstNumber > secondNumber{
            print(firstNumber, "Is bigger")
        } else {
            print(secondNumber, "Is bigger")
        }
        
        // Block 1 Task 1
        print("-------Block 1 Task 1:-------")
        let numberInTask1 = 13
        func numberInSquareFunction() -> Int {
            let NumberInSquare = numberInTask1*numberInTask1
            return NumberInSquare
        }
        let NumberInCube = numberInSquareFunction()*numberInTask1
        print("Number in square equal", numberInSquareFunction())
        print("Number in cube equal", NumberInCube)
        
        // Block 1 Task 2
        print("-------Block 1 Task 2:-------")
        let numberInTask2 = 5
        for fromZero in 0...numberInTask2 {
            print(fromZero)
        }
        for toZero in -numberInTask2...0{
            print(-toZero)
        }
        
        // Block 1 Task 3
        print("-------Block 1 Task 3:-------")
        let numberInTask3 = 6
        for delitel in 1...numberInTask3 {
            if numberInTask3 % delitel == 0 {
                print(delitel)
            }
        }
        
        // Block 1 Task 4
        print("-------Block 1 Task 4-------")
        let number = 496
        var S = 1
        for delitel in 2...number-1 {
            if number % delitel == 0 {
                S += number / delitel + delitel
                print(delitel)
                if S==number {
                    print("!!!--СОВЕРШЕННОЕ--!!!")
                }
            }
        }
        
        // Block 2 Task 1
        print("-------Block 2 Task 1-------")
        var startPrice:Double = 24
        let everyYear:Double = 1.06
        let years = 192
        for _ in 1...years{
            startPrice=startPrice*everyYear
            
        }
        print(startPrice,"$")
        
        //Block 2 Task 2
        print("-------Block 2 Task 2-------")
        let stependia: Double = 700
        var costsOfFirstMonth:Double = 1000
        let quantityOfMonth: Double = 10
        func CalculateAllStependiaForFewMonth() -> Double{
            let stependiaForFewMonth = stependia * quantityOfMonth
            return stependiaForFewMonth
        }
        var costs:Double = 0
        let inMonth = 1.03
        for _ in 0..<10{
            costs=costs+costsOfFirstMonth
            costsOfFirstMonth=costsOfFirstMonth*inMonth
        }
        let shouldAskFor = costs-CalculateAllStependiaForFewMonth()
        print("Степендия за 10 мес:", CalculateAllStependiaForFewMonth())
        print("Траты за 10 мес:", costs)
        print("Нужно взять с собой:", shouldAskFor)
        
        // Block 2 Task 3
        print("-------Block 2 Task 3-------")
        let stepuha: Double = 700
        var savings: Double = 2400
        let RostCen = 0.03
        var monthUse = 1000.0
        var useInMonth = 0.0
        var amountMonth = 0
        func howManyMonthStudentWillLive() -> Int{
            while savings > useInMonth {
                savings = savings + stepuha
                monthUse = monthUse + monthUse * RostCen
                useInMonth = useInMonth + monthUse
                amountMonth = amountMonth + 1
            }
            return amountMonth
        }
        
        print("Student will survive by \(howManyMonthStudentWillLive()) month")
        
        // Block 2 Task 4
        print("-------BLock 2 Task 4-------")
        let a = 675
        var b = 0
        var  temp = a
        while b < a {
            b = b * 10 + temp % 10
            temp/=10
            print(b)
        }
    }
    
}







